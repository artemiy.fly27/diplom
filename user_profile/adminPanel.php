<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/auth/security.php";
RequireRole(R_REGISTERED) ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin panel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/user_profile/adminPanel.css">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body class="grey lighten-3">

<?php require $_SERVER["DOCUMENT_ROOT"]."/common/header.php" ?>
<div class="w3-sidebar w3-light-grey w3-bar-block" style="width:180px"> <!-- TODO: rolling sidebar-->
    <?php
        if (($_SESSION["role"] & R_MODERATOR) != 0) {
            echo "<a href=\"/user_profile/admin/services.php\" class=\"w3-bar-item w3-button\">Исполнители</a>";
            echo "<a href=\"/user_profile/admin/spisok_masterov_dla_adminki.php\" class=\"w3-bar-item w3-button\">Все заказы</a>";
        }
    ?>
    <a href="/user_profile/personal_request_list.php" class="w3-bar-item w3-button">Мои заказы</a>
    <form action="/" >
        <input type="hidden" name="sign_out">
        <input type="submit" class="btn btn-amber w-75 m-4" value="Выйти">
    </form>
</div>
<div style="margin-left:180px">
<?php echo $content ?>

</div>

</body>
</html>


