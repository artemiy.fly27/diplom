<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/auth/security.php";
RequireRole(R_MODERATOR);

$conn = GetDB();

if (isset($_POST["target"])) {
    $target = $_POST["target"];
    $sql = "UPDATE services SET";
    $keys = array("name", "phone", "short_descr", "long_descr");
    $i = 0;
    foreach ($keys as $tag) {
        if (isset($_POST[$tag])) {
            if ($i > 0) {
                $sql .= ',';
            }
            $sql .= " ".$tag."='".$_POST[$tag]."'";
            ++$i;
        }
    }
    if (isset($_FILES["photo"])) {
        $check = getimagesize($_FILES["photo"]["tmp_name"]);
        if ($check !== false) {
            $token = NewToken();
            if ($_FILES["fileToUpload"]["size"] > 500000) {
                $too_big_picture = true;
            } else {
                $dest = "../../web/img/".$token;
                move_uploaded_file($_FILES["photo"]["tmp_name"], $dest);
                if ($i > 0) {
                    $sql .= ',';
                }
                $sql .= " photo='" . $token."'";
                ++$i;
            }
        }
    }
    if ($i > 0) {
        $sql .= " WHERE id=".$target;
        $conn->query($sql);
    }

    $sql = "SELECT name, phone, photo, short_descr, long_descr FROM services
        WHERE id=".$_POST["target"];
    $result = $conn->query($sql);
    if ($result->num_rows == 0) {
        echo "USER NOT FOUND";
        exit;
    }

    $row = $result->fetch_assoc();
    $target = $_POST["target"];
    $name = $row["name"];
    $phone = $row["phone"];
    $photo = $row["photo"];
    $short_descr = $row["short_descr"];
    $long_descr = $row["long_descr"];
} else {

    $photo = "default.png";
    $sql = "INSERT INTO services(name, photo, phone, short_descr, long_descr) VALUES ('', '".$photo."', '', '', '')";
    $result = $conn->query($sql);
    $result = $conn->query("SELECT LAST_INSERT_ID()");
    $row = $result->fetch_array();
    $target = $row[0];
    $name = "";
    $phone = "";
    $short_descr = "";
    $long_descr = "";
}
$conn->close();
$content = "
<div class=\"container\">
    <div class=\"container bootstrap snippet\">
        <div class=\"row\">
            <div class=\"col-sm-10\"><h1>profile performer</h1></div>
            <div class=\"col-sm-2\"></div>
        </div>
        <div class=\"row\">
            <form class=\"form\" method=\"post\" id=\"registrationForm\" enctype=\"multipart/form-data\">
                <div class=\"col-sm-3\"><!--left col-->
    
    
                    <div class=\"text-center\">
                        <img src=\"".PHOTO_DIR.$photo."\" class=\"avatar img-circle img-thumbnail\" alt=\"avatar\">
                        <h6>Upload a different photo...</h6>
                        <input type=\"file\" name =\"photo\" class=\"text-center center-block file-upload\">
                    </div>
                    <br>
    
                </div><!--/col-3-->
                <div class=\"col-sm-9\">
                    <div class=\"tab-content\">
                        <div class=\"tab-pane active\" id=\"home\">
                            <hr>
                                <input type='hidden' name='target' value='".$target."'>
                                <div class=\"form-group\">
    
                                    <div class=\"col-xs-6\">
                                        <label for=\"first_name\"><h4>Название</h4></label>
                                        <input type=\"text\" class=\"form-control\" name=\"name\" id=\"first_name\" value=\"".$name."\" title=\"enter your first name if any.\">
                                    </div>
                                </div>
                           
                                <div class=\"form-group\">
    
                                    <div class=\"col-xs-6\">
                                        <label for=\"phone\"><h4>Phone</h4></label>
                                        <input type=\"text\" class=\"form-control\" name=\"phone\" id=\"phone\" value=\"".$phone."\" title=\"enter your phone number if any.\">
                                    </div>
                                </div>
    
                                <div class=\"form-group\">
    
                                    <div class=\"col-xs-6\">
                                        <label for=\"phone\"><h4>Короткое описание</h4></label>
                                        <input type=\"text\" class=\"form-control\" name=\"short_descr\" value=\"".$short_descr."\" title=\"не больше 10 слов о себе\">
                                    </div>
                                </div>
                                <div class=\"form-group\">
    
                                    <div class=\"col-xs-6\">
                                        <label for=\"phone\"><h4>Полное описание</h4></label>
                                        <textarea type=\"text\" class=\"form-control\" name=\"long_descr\" >".$long_descr."</textarea>
                                    </div>
                                </div>
    
                                <div class=\"form-group\">
                                    <div class=\"col-xs-12\">
                                        <br>
                                        <button class=\"btn btn-lg btn-success\" type=\"submit\"><i class=\"glyphicon glyphicon-ok-sign\"></i> Save</button>
                                        <button class=\"btn btn-lg\" type=\"reset\"><i class=\"glyphicon glyphicon-repeat\"></i> Reset</button>
                                    </div>
                                </div>
    
                            <hr>
    
                        </div><!--/tab-pane-->
                        
                </div><!--/tab-content-->
            </form> 
        </div>";
require "../adminPanel.php"
?>