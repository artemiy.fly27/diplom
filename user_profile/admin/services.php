<?php
require_once "../../auth/security.php";

RequireRole(R_MODERATOR);

$conn = GetDB();

if (isset($_POST["to_delete"])) {
    $sql = "DELETE FROM services WHERE id=".$_POST["to_delete"];
    $conn->query($sql);
}

$content = "
<form method=\"post\" name='delf'>
  <input type='hidden' name=\"to_delete\" value=\"\">
</form>
<script>
    function del(id, name) {
        if (confirm('Удалить ' + name + '?')) {
            form = document.forms.delf;
            form.elements.to_delete.value = id;
            form.submit(); 
        }
    }
</script>
<a href='edit_profile.php'><button type=\"button\" class=\"btn btn-outline-danger\" style='margin-left: 200px'>Новый профиль</button></a>
<br>";

$result = $conn->query("SELECT * FROM services");

while ($row = $result->fetch_assoc()) {
    $content .= "
    <div class='row w3-border p-3 m-5'>
      <img src=\"".PHOTO_DIR.$row["photo"]."\" style='width: 150px; height: 150px'; class='col-2' alt=\"avatar\">
               <p  class=\"textarea col-8\">
               ".$row["name"]."<br>
               ".$row["short_descr"]."
               </p >
    
       <form action='edit_profile.php' method='post'> <input type='hidden' name='target' value='".$row["id"]."'>
       <button type=\"submit\" class=\"btn btn-primary\" style='width: 90px; height: 50px; margin-top: 30px; '><span class=>Править</span></button>
       </form>
       <button onclick='del(".$row["id"].", \"".$row["name"]."\")' type=\"submit\" class=\"btn btn-danger\" style='width: 55px; height: 55px; margin-left: 50px; margin-top: 25px'><span>&#10006;</span></button>
   </div>";
}

$conn->close();

require "../adminPanel.php"
?>