


<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/auth/security.php";
RequireRole(R_REGISTERED);


$conn = GetDB();

if ($_REQUEST["action"] == "place_orders") {
    $sql = "SELECT id FROM orders WHERE token = '%s' AND status=%d";
    $sql = sprintf($sql, $_COOKIE["TOKEN"], ST_IN_BASKET);
    $result = $conn->query($sql);
    $client = $_REQUEST["name"]."; ".$_REQUEST["phone"]."; ".$_REQUEST["address"];
    while ($row = $result->fetch_array()) {
        $sql = "UPDATE orders SET comment='%s', status=%d, last_update=CURRENT_TIMESTAMP(), client='%s' WHERE id=%d";
        $sql = sprintf($sql, $_REQUEST["comment-".$row[0]], ST_ORDERED, $client, $row[0]);
        $conn->query($sql);
    }
}

if ($_REQUEST["action"] == "place_single_order") {
    $sql = "INSERT INTO orders(comment, status, last_update, client, token, service) VALUES ('%s', %d, CURRENT_TIMESTAMP(), '%s', '%s', %d)";
    $sql = sprintf($sql, $_REQUEST["comment"], ST_ORDERED, $_REQUEST["name"]."; ".$_REQUEST["phone"]."; ".$_REQUEST["address"], $_COOKIE["TOKEN"], $_REQUEST["id"]);
    $result = $conn->query($sql);
}

$sql = "SELECT * FROM orders WHERE token = '%s' AND status=%d";
$sql = sprintf($sql, $_COOKIE["TOKEN"], ST_ORDERED);
$result = $conn->query($sql);

$content = "
<script>
    function setRaiting(id, val) {
        var xr = new XMLHttpRequest();
        xr.open('GET', '/shop/impl.php?action=rank&rating=' + val + '&id='+id);
        xr.send();
    }
</script>
";

while ($row = $result->fetch_assoc()) {
    $sql = "SELECT name, photo, phone, short_descr FROM services WHERE id=".$row["service"];
    $subrow = $conn->query($sql)->fetch_assoc();
    $content .= "
    <div class=\"row m-3 border p-3 border-success\">
        <div class=\"text-center border border-success mr-3\" >
            <img src=\"".PHOTO_DIR.$subrow["photo"]."\" class=\"avatar img-circle img-thumbnail h-100\" alt=\"avatar\">
        </div>
        <div class=\"col-3\">".$subrow["name"]."<br>
        ".$subrow["phone"]."<br>
        ".$subrow["short_descr"]."
        </div>
        <p class='m-3 col-6'>".$row["comment"]."</p>
        
        <div id='div-".$row["id"]."' onmouseout=\"Select_star_".$row["id"]."(-1);\" style='align-self: center; font-size: x-large'></div>
    </div>
    <script>
        var mark_".$row["id"]."=".$row["rating"].";
        var star_div_".$row["id"]." = document.getElementById('div-".$row["id"]."');
        for (let i = 0; i < 5; i++) {
            star = document.createElement('span');
            star.onclick = function () { Select_star_".$row["id"]."(i + 1, true); };
            star.onmouseover = function () { Select_star_".$row["id"]."(i + 1); };
            star.innerHTML = \"★\";
            star_div_".$row["id"].".appendChild(star);
	    }
        
        function Select_star_".$row["id"]."(val, do_fix = false) {
            if (do_fix) {
                mark_" . $row["id"] . " = val;
                setRaiting(".$row["id"].", val);
            } else if (val < 0) 
                val = mark_".$row["id"].";
            Array.from(document.querySelectorAll(\"#div-".$row["id"]." span\")).forEach(function (cur, i) {
                cur.style.color = (i >= val) ? \"#000\" : \"#F00\";
            });
        };
        Select_star_".$row["id"]."(-1);
    </script>
    ";
}

$conn->close();

require "adminPanel.php"
?>