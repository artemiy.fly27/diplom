<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>my project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/releases/v5.8.2/css/all.css"></script>
    <link rel="stylesheet" href="specialist_profile.css">
</head>
<body>
<!--подключаем шапку-->
<?php require "../common/header.php" ?>
<!--подключаем шапку-->
<div class="layout-responsive">
    <div id="BaseProfileContainer" class="layout-responsive__inner layout-responsive__inner--main layout-profile js-el">
        <div class="layout-profile__column layout-profile__column--left">

            <div id="GalleryContainer" class="hidden js-page js-page-gallery"></div>

            <div id="userProfile">
                <div class="js-block-summary-info">



                    <div class="b-profile-block b-profile-block--left b-profile-block__summary_info">



                        <h1 class="user-name">
                            Здравствуйте, меня зовут Василич

                        </h1>

                        <table class="table table-silver ">
                            <tr>
                                <td>
                                <div class="user-avatar">
                                    <div class="b-avatar b-avatar--s180 b-avatar--s140-m">
                                        <img class="js-userPhoto"  src="/web/img/default.png" style="height: 345px" alt="">
                                    </div>
                                </div>
                                </td>
                                <td>
                                    <p  type="text"  style="text-align:center "> </p>
                                    <p  type="text"  style="text-align: center"> </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="profile-userbuttons">
                                       <td><button type="button" class="btn btn-outline-success btn-sm" style="margin-left: 750px">в 1 клик</button></td>
                                        <td><button type="button" class="btn btn-outline-success btn-sm">подробнее об услуге</button></td>
                                    </div>
                                </td>
                            </tr>
                        </table>


                        <div class="hidden" id="badges__verified-passport"><div class="i-center">Исполнитель пока не подтвердил свой паспорт.</div></div>

                        <div class="user-data">

                            <div class="user-info">

                            </div>


                            <div class="user-tasks">
                                Создал 1 задание		</div>

                            <div class="user-rating">
                                <span>Нет оценок</span>
                            </div>



                        </div>

                    </div>

                </div>

                <div class="b-block-user-certificates js-block-user-certificates hidden"></div>
                <div class="hidden js-block-user-navigation"></div>
                <div class="b-profile-wrapper js-profile-content-wrapper" style="position: relative;">
                    <div id="ProfileContainer" class="js-page js-page-profile">


                        <div class="js-publish-wrapper">



                        </div>
                        <div class="js-anketa-wrapper">


                            <form class="b-profile-block b-profile-block--left b-profile-block__anketa b-profile-block__verification_request_form" novalidate="novalidate" unobtrusive="skip">



                                <div class="b-profile-block__anketa__row js-editable-area">


                                </div>

                            </form>

                        </div>

                        <div class="js-scores-wrapper">
                            <!-- tpl/views/profile/Scores.ejs -->


                            <div class="b-profile-block b-profile-block--left txt-block">

                                <h4 class="b-profile-block__title">

                                    У Мастера нет оценок в настоящий момент

                                </h4>

                                <p class="b-profile-block__description">
                                    Заказчики оценивают всех исполнителей по разнообразным критериям таким как цена, качество, вежливость
                                </p>

                            </div>



                        </div>

                        <div class="js-reviews-wrapper">



                            <div class="b-profile-block b-profile-block--left b-profile-block__no_reviews">

                                <h4 class="b-profile-block__title">Отзывов пока нет</h4>

                                <p class="b-profile-block__description">Отзывы появятся после того, как пользователь создаст или выполнит задание</p>

                            </div>

                        </div>
                        <div class="js-verification-request-wrapper"></div>

                    </div>
                    <div id="WalletContainer" class="hidden js-page js-page-wallet"></div>
                    <div id="FavoritesContainer" class="hidden js-page js-page-favorites"></div>
                    <div class="js-profile-settings-wrapper">
                        <div id="SettingsPersonalContainer" class="hidden js-page js-page-settings-personal"></div>
                        <div id="SettingsNotificationsContainer" class="hidden js-page js-page-settings-notifications"></div>
                        <div id="SettingsSubscribeContainer" class="hidden js-page js-page-settings-subscribe"></div>
                    </div>
                </div>
            </div>

        </div>

</div>

</body>
<div class="feedback_form_bg">
    <form method="post" action="javascript:void(0);" onsubmit="send_form();" id="forma">
        <input type="text" name="fio" placeholder="Ф.И.О" />
        <input type="text" name="phone" placeholder="Телефон" />
        <input type="text" name="email" placeholder="E-Mail" />
        <input type="text" name="adres" placeholder="Адрес доставки" />
        <br/>
        <input type="submit" value="отправить" />
    </form>
</div>
</html>