<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
    <div class="container-fluid">
        <a href="#" class="navbar-brad"><img class="w-75" src="/web/img/header.png"> </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarResponsive">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="/shop/Categorii_specialista.php" class="nav-link waves-effect">Магазин</a>

                </li>
                <li class="nav-item">
                    <a href="/shop/basket.php" class="nav-link">Корзина</a>

                </li>
                 <li class="nav-item">
                    <?php
                    require_once $_SERVER['DOCUMENT_ROOT']."/auth/security.php";
                    if (isset($_SESSION["username"])) {
                        echo "<a href=\"/user_profile/adminPanel.php\" class=\"nav-link\"><b>".$_SESSION["username"]."</b></a>";
                    } else {
                        echo "<a href=\"/auth/authorization.php\" class=\"nav-link\"><b>Войти</b></a>
                                    </li><li class=\"nav-item\">
                                  <a href=\"/auth/registration.php\" class=\"nav-link\">Зарегистрироваться</a>";
                    }
                    ?>

                </li>
            </ul>

        </div>
    </div>
</nav>
