<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>my project</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
		  crossorigin="anonymous">

	<link rel="stylesheet" href="Categorii_specialista.css">
    <script>
        function openForm(id) {
            document.forms.popup.elements.id.value = id;
            document.getElementById("popup_bg").style.display = "block";
        }

        function closeForm() {
            document.getElementById("popup_bg").style.display = "none";
        }
        function addToBasket(id, obj) {
            obj.innerHTML = "Убрать";
            var xr = new XMLHttpRequest();
            xr.open('GET', 'impl.php?action=add_to_basket&service='+id);
            xr.send();
            obj.onclick = function () { removeFromBasket(id, obj) };
        }
        function removeFromBasket(id, obj) {
            obj.innerHTML = "В корзину";
            var xr = new XMLHttpRequest();
            xr.open('GET', 'impl.php?action=remove_from_basket&service='+id);
            xr.send();
            obj.onclick = function () { addToBasket(id, obj) };
        }
    </script>
</head>
<body>
<?php require "../common/header.php" ?>

	<header>


		<br>
			<section id="services">
		<div class="container">
			<div class="title">
				<h2 id="headLine1">
					Categories
				</h2>
				<p>
					выборка специалиста
				</p>
			</div>
			<div class="services clearfix">
				<div class="services__item">
					<img src="../web/img/elektrika.png" id="elektrika1" style = "padding: 0 0 0 10px;">
					<h3>электрик</h3>
					<p>
						Именно этого специалиста мы вызываем сразу же, как только начинает искрить розетка или перестает работать выключатель, когда нужно повесить новую люстру или произвести замену электропроводки в доме.

					</p>
				</div>
				<div class="services__item">
					<img src="../web/img/Brigada1.jpg" >
					<h3>бригада</h3>
					<p>Одной из наиболее распространенных форм организации труда в строительной отрасли является строительная бригада. Бригада дает возможность рационально использовать каждого работника в соответствии с его квалификацией и трудоспособностью. </p>
				</div>
				<div class="services__item">
					<img src="../web/img//santexnik2.jpg" >
					<h3>сантехник</h3>
					<p>
						На плечах этих специалистов лежит ответственность за полноценное функционирование систем центрального отопления, горячего и холодного водоснабжения, канализации, газоснабжения промышленных и гражданских сооружений.
					</p>
				</div>
			</div>
		</div>
	</section>


		<section class="listings">
			<div class="wrapper">
				<ul class="properties_list">

<?php

$conn = GetDB();
$sql = "SELECT id, name, photo, short_descr FROM services";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
echo                "<li>
						<a href = \"#\" >
							<img src = \"".PHOTO_DIR.$row["photo"]."\" alt = \"\" title = \"\" class=\"property_img\" />
						</a >
						<div class=\"property_details\" >
							<h1 >
							    ".$row["name"]."
							</h1 ><br>
							".$row["short_descr"]."
							<a href = \"magazine.php?id=".$row["id"]."\" class=\"btn btn-warning m-2\" > узнать о специалисте </a >
							<div class=\"profile-userbuttons\" >
								<button type = \"button\" class=\"btn btn-danger btn-sm border border-danger\" onclick='openForm(".$row["id"].")'> Заказать </button >
								<button type = \"button\" class=\"btn btn-danger btn-sm border border-danger\" onclick='addToBasket(".$row["id"].", this)'> В корзину</button >
							</div >
						</div >
					</li >";
}
?>
				</ul>
			
			</div>
		</section>

	<!--  end listing section  -->

<div class="feedback_form_bg" onclick="closeForm()" id="popup_bg">
	<form method="post" action="/user_profile/personal_request_list.php" id="forma" name="popup" onclick="event.stopPropagation()">
        <input type="hidden" name="action" value="place_single_order">
        <input type="hidden" name="id" value="">
		<input type="text" name="name" placeholder="Ф.И.О">
		<input type="text" name="phone" placeholder="Телефон">
        <input type="text" name="address" placeholder="Адрес">
        <input name="comment" placeholder="Комментарий" >
		<br>
		<input type="submit" value="отправить">
	</form>
</div>
</body>
</html>