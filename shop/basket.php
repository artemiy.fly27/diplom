<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="basket.css" >
    <title>Title</title>
    <script>
        function removeFromBasket(id) {
            var xr = new XMLHttpRequest();
            xr.open('GET', 'impl.php?action=remove_from_basket&service='+id);
            xr.send();
            elem = document.getElementById("div-"+id);
            elem.parentNode.removeChild(elem);
        }
    </script>
</head>

<body>
<!--подключаем шапку-->
<?php require "../common/header.php" ?>
<!--подключаем шапку-->
<form action="/user_profile/personal_request_list.php" method="post">
    <input type="hidden" name="action" value="place_orders">
<?php
$conn = GetDB();
$sql = "SELECT id, service FROM orders WHERE status=%d AND token='%s'";
$sql = sprintf($sql, ST_IN_BASKET, $_COOKIE["TOKEN"]);
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    $sql = "SELECT name, photo, short_descr FROM services WHERE id=".$row["service"];
    $subrow = $conn->query($sql)->fetch_assoc();
    echo "
    <div class=\"row m-3 border p-3 border-success\" id='div-".$row["service"]."'>
            <div class=\"text-center border border-success mr-3\" >
                <img src=\"".PHOTO_DIR.$subrow["photo"]."\" class=\"avatar img-circle img-thumbnail h-100\" alt=\"avatar\">
            </div>
            <div class=\"col-3\">
            ".$subrow["name"]."<br>
            ".$subrow["short_descr"]."<br>
            </div>
            <textarea  name=\"comment-".$row["id"]."\" class=\"textarea col-6\"></textarea >
    
             <button type=\"submit\" form='' onclick='removeFromBasket(".$row["service"].")' class=\"btn btn-danger m-5\" style=\"right: 50px; ;width: 50px; height: 50px\"><span class=>&#10006;</span></button>
    </div>";
}

$conn->close();
?>
<table class="table bg-light table-hover">
    <tr>
        <td>Имя</td>
        <td><input type="text" name="name" placeholder="Ф.И.О" /></td>
    </tr>
    <tr>
        <td>Адрес</td>
        <td><input type="text" name="address" placeholder="Адрес" /></td>
    </tr>
    <tr>
        <td>контактный телефон</td>
        <td><input type="text" name="phone" placeholder="Телефон" /></td>
    </tr>
</table>
<div>
    <td><button type="submit" class="btn btn-danger " style="width: 150px; height: 50px; margin-left: 1400px"><span class=>Оформить заказ</span></button></td>
</div>
</form>


</body>
</html>