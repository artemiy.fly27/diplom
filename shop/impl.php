<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/auth/security.php";

$conn = GetDB();

if ($_REQUEST["action"] == "add_to_basket") {
    $sql = "INSERT INTO orders(token, service, status) VALUES ('%s', '%s', %d)";
    $sql = sprintf($sql, $_COOKIE["TOKEN"], $_REQUEST["service"], ST_IN_BASKET);
    $conn->query($sql);
}

if ($_REQUEST["action"] == "remove_from_basket") {
    $sql = "DELETE FROM orders WHERE service=%d AND token='%s' AND status=%d";
    $sql = sprintf($sql, $_REQUEST["service"], $_COOKIE["TOKEN"], ST_IN_BASKET);
    $conn->query($sql);
}

if ($_REQUEST["action"] == "rank") {
    $sql = "UPDATE orders SET rating=%d WHERE id=%d AND token='%s'";
    $sql = sprintf($sql, $_REQUEST["rating"], $_REQUEST["id"], $_COOKIE["TOKEN"]);
    echo $sql;
    $conn->query($sql);
}

$conn->close();