<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="magazine.css" >
    <title>Title</title>
    <script>
        function openForm(id) {
            document.forms.popup.elements.id.value = id;
            document.getElementById("popup_bg").style.display = "block";
        }

        function closeForm() {
            document.getElementById("popup_bg").style.display = "none";
        }
        function addToBasket(id, obj) {
            obj.innerHTML = "Убрать";
            var xr = new XMLHttpRequest();
            xr.open('GET', 'impl.php?action=add_to_basket&service='+id);
            xr.send();
            obj.onclick = function () { removeFromBasket(id, obj) };
        }
        function removeFromBasket(id, obj) {
            obj.innerHTML = "В корзину";
            var xr = new XMLHttpRequest();
            xr.open('GET', 'impl.php?action=remove_from_basket&service='+id);
            xr.send();
            obj.onclick = function () { addToBasket(id, obj) };
        }
    </script>
</head>

<body>
<!--подключаем шапку-->
<?php require "../common/header.php";
$conn = GetDB();
$sql = "SELECT AVG(rating) FROM orders WHERE rating>0 AND service=".$_REQUEST["id"];
$rating = $conn->query($sql)->fetch_array()[0];
$row = $conn->query("SELECT * FROM services WHERE id=".$_REQUEST["id"])->fetch_assoc();
$conn->close();
?>
<!--подключаем шапку-->
<table class="table table-btn btn-link">
    <!--magazine.php -->
    <tr>

        <td>
            <form style="margin-left: 1050px">
<span class="input-number ">
</span>
                <a rel="nofollow" href="/shop/basket.php" class="btn-default">В корзину</a>
            </form>
        </td>
        </div>
    </tr>
    <!--magazine.php -->
</table>
<div class="container rounded bg-light border border-success ">
    <div class="row profile bg-light"  >
        <div class="col-md-11">
            <div class="profile-sidebar bg-light">
                <!-- сайдбар пользователя -->
                <div class="profile-userpic row">
                    <div class="col-3" style="text-align: center; font-size: x-large">
                    <img src="<?php echo PHOTO_DIR.$row["photo"] ?>" class="navbar-brad"  alt=""><br>
                    <p class="m-2">
                    Cредний балл: <?php echo number_format($rating, 1)  ?>
                    </p>
                    </div>
                    <div class="col-2"></div>
                    <div>
                    <p class="text-md" style="margin-left: 20px"><h2><?php echo $row["name"] ?> </h2> </p><br>
                    <?php echo $row["phone"] ?><br>
                    <?php echo $row["short_descr"] ?>
                    </div>
                </div>

                <!-- сайдбар пользователя -->

                <!-- текст под сайдбаром -->
                <br>
                    <div class="text-center w-100">
                        <?php echo $row["long_descr"] ?>
                    </div>
                <!-- текст под сайдбаром -->
                <!-- сайдбар, кнопки -->

                <div class="profile-userbuttons">
                    <button type="button" class="btn btn-danger btn-sm border border-danger" onclick="openForm(<?php echo $_REQUEST["id"]?>)">Быстрый заказ</button>
                    <button type="button" class="btn btn-danger btn-sm border border-danger" onclick="addToBasket(<?php echo $_REQUEST["id"]?>, this)">В корзину</button>
                </div>

                <!-- сайдбар кнопки -->

                <!-- END MENU -->
            </div>

        </div>
    </div>
</div>
<div class="feedback_form_bg" onclick="closeForm()" id="popup_bg">
    <form method="post" action="/user_profile/personal_request_list.php" id="forma" name="popup" onclick="event.stopPropagation()">
        <input type="hidden" name="action" value="place_single_order">
        <input type="hidden" name="id" value="">
        <input type="text" name="name" placeholder="Ф.И.О">
        <input type="text" name="phone" placeholder="Телефон">
        <input type="text" name="address" placeholder="Адрес">
        <input type="text" name="comment" placeholder="Комментарий" >
        <br>
        <input type="submit" value="отправить">
    </form>
</div>


</body>

</html>