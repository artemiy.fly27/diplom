<?php
    if (isset($_REQUEST["sign_out"])) {
        unset($_SESSION["role"]);
        unset($_SESSION["username"]);
        unset($_COOKIE["TOKEN"]);
        setcookie("TOKEN", "", time() - 3600, '/');
    }
?>
<html>
<head>
    <title>Some Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin panel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">

</head>
<body>
<?php require "common/header.php" ?>
</body>
</html>