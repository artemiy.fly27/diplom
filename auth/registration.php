<?php
require "utils.php";

if (isset($_POST["username"])) {
    if (mb_strlen($_POST["password"]) <= 5) {
        $short_password = true;
    } else {;
        $conn = GetDB();
        if ($conn->connect_error) {
            echo("Connection failed: " . $conn->connect_error);
        }
        $token = NewToken();
        $pattern = "INSERT INTO users
            (username, password, token, role, firstname, lastname)
            VALUES ('%s', '%s', '%s', %d, '%s', '%s')";
        $a = sprintf($pattern, $_POST["username"], password_hash($_POST["password"], PASSWORD_DEFAULT), $token, R_REGISTERED, $_POST["firstname"], $_POST["lastname"]);
        $result = $conn->query($a);
        $conn->close();
        if ($result) {
            $_SESSION["role"] = R_REGISTERED;
            $_SESSION["user"] = $_POST["username"];
            setcookie("TOKEN", $token, 0, "/");
            if (isset($_REQUEST["source"])) {
                header("Location: " . $_REQUEST["source"], true, 302);
                exit;
            } else {
                header("Location: /", true, 302);
                exit;
            }
        } else {
            $username_exists = true;
        }
    }

}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bootstrap 4 Introduction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="auth.css">

</head>

<body>
<?php require "../common/header.php" ?>
<form method="post" class="main-form needs-validation" novalidate>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="firstname">Firstname</label>
                <input type="text" name="firstname" id="firstname" class="form-control"
                    <?php if (isset($_POST["firstname"])) echo "\" value = \"".$_POST["firstname"]."\"" ?>>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="lastname">Lastname</label>
                <input type="text" name="lastname" id="lastname" class="form-control"
                    <?php if (isset($_POST["lastname"])) echo "\" value = \"".$_POST["lastname"]."\"" ?>>
                <small class="form-text text-muted">
                    This should be your last name.
                </small>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="username">Username</label>
        <input type="text" name="username" id="username" class="form-control
        <?php if ($username_exists) echo " is-invalid";
        if (isset($_POST["username"])) echo "\" value = \"".$_POST["username"] ?>" required>
        <?php if ($username_exists) echo "<div class=\"invalid-feedback\">Имя пользователя уже занято</div>"?>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" id="password" class="form-control
                 <?php if ($short_password) echo " is-invalid"?>">
        <?php if ($short_password) echo "<div class=\"invalid-feedback\">Пароль должен содержать по крайней мере 6 знаков</div>"?>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

</body>

</html>