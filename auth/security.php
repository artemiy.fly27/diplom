<?php
require_once "utils.php";
if (!isset($_SESSION["role"]) && isset($_COOKIE["TOKEN"])) {
    $conn = GetDB();
    if ($conn->connect_error) {
        echo("Connection failed: " . $conn->connect_error);
    }
    $a = "SELECT username, role FROM users WHERE token = '" . $_COOKIE["TOKEN"] . "'";
    $result = $conn->query($a);
    if ($result->num_rows > 0) {
        // output data of each row
        $row = $result->fetch_assoc();
        $_SESSION["role"] = $row["role"];
        $_SESSION["username"] = $row["username"];
    } else {
        $_SESSION["role"] = R_GUEST;
        setcookie("TOKEN", New  Token(), 0, '/');
    }
    $conn->close();
}

function RequireRole($roles_mask) {
    if (($_SESSION["role"] & $roles_mask) !== $roles_mask) {
        echo "ACCESS DENIED";
        exit;
    }
}
