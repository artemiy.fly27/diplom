<?php
    if (isset($_POST["username"])) {
        require "utils.php";
        $conn = GetDB();
        if ($conn->connect_error) {
            echo("Connection failed: " . $conn->connect_error);
        }
        $a = sprintf("SELECT role, token, password FROM users WHERE username = '%s'", $_POST["username"]);
        $result = $conn->query($a);
        if ($result->num_rows > 0) {
            // output data of each row
            $row = $result->fetch_assoc();
            if (!password_verify($_POST["password"], $row["password"])) {
                $wrong_user = true;
            } else {
                $_SESSION["role"] = $row["role"];
                $_SESSION["user"] = $_POST["username"];
                setcookie("TOKEN", $row["token"], 0, "/");
                $conn->close();
                if (isset($_REQUEST["source"])) {
                    header("Location: " . $_REQUEST["source"], true, 302);
                    exit;
                } else {
                    header("Location: /", true, 302);
                    exit;
                }
            }
        } else {
            $wrong_user = true;
        }
        $conn->close();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bootstrap 4 Introduction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="auth.css">

</head>

<body>

<?php require "../common/header.php" ?>
<form method="post" class="main-form needs-validation" novalidate>
    <div class="form-group">
        <label for="username">Username</label>
        <input type="text" name="username" id="username" autocomplete="username" class="form-control
                 <?php if ($wrong_user) echo " is-invalid"?>" required
                <?php if (isset($_POST["username"])) echo "value=".$_POST["username"] ?>>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" autocomplete="password" id="password" class="form-control
         <?php if ($wrong_user) echo " is-invalid"?>"required >
        <?php
            if ($wrong_user) {
                echo '<div class="invalid-feedback">Неправильный логин или пароль.</div>';
            }
        ?>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

</body>

</html>